﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using HtmlAgilityPack;

namespace ConsoleApp4
{
    class Program
    { 
        static void Main(string[] args)
        
        {
            for (var page = 0; page < 1; page++)
            {
                var firstUrl = $@"https://www.avito.ru/sankt-peterburg/kvartiry/prodam/novostroyka?p={page}";
                var web = new HtmlWeb {UseCookies = true};
                var htmlDoc = web.Load(firstUrl);
                List<string> list = new List<string>();
                var urls = htmlDoc.DocumentNode.SelectNodes("//div/h3/a[@class='item-description-title-link']");
                page++;
                foreach (var url in urls)
                {
                    var urlAtr = url.GetAttributeValue("href", null);
                    list.Add(urlAtr);
                }
                foreach (string lis in list)
                {
                    var sitem = $@"https://www.avito.ru{lis}";
                    var sWeb = new HtmlWeb {UseCookies = true};
                    var htmlDocItem = sWeb.Load(sitem);
                    List<string> itemlist = new List<string>();
                    var itemNodes = htmlDocItem.DocumentNode.SelectNodes("//li[@class='item-params-list-item']")
                        .Nodes();
                    var priceXpath = "//span[@class='js-item-price']";
                    var picCountXpath = "//span[@class='gallery-list-item-link']";
                    var addresXpath = "//span[@class='item-address__string']";
                    var titleXpath = "//h1/span[@class='title-info-title-text']";
                    foreach (var itemNode in itemNodes)
                    {
                        itemlist.Add((itemNode.InnerHtml.Replace("&nbsp;", " ")));
                    }
                    var price = htmlDocItem.DocumentNode.SelectNodes(priceXpath).First().InnerText;
                    var picCountCalc = (htmlDocItem.DocumentNode.SelectNodes(picCountXpath));
                    var picCount = picCountCalc?.Count ?? 0;
                    var addres = htmlDocItem.DocumentNode.SelectSingleNode(addresXpath).InnerText;
                    var title = htmlDocItem.DocumentNode.SelectSingleNode(titleXpath).InnerText;
                    StreamWriter file = new StreamWriter(@"C:\Users\myTigran\Desktop\file.txt", true);
                    file.WriteLine(title);
                    for (int i = 0; i < itemlist.Count; i++)
                    {
                        file.WriteLine(itemlist[i]);
                    }
                    file.WriteLine("Цена: " + price + " рублей");
                    file.WriteLine("Колличество фотографий в объявлении: " + picCount);
                    file.WriteLine("Адрес: " + addres);
                    file.Close();
                }
            }

        }
    }
}
    
